/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kodilla.tictactoe;

import javax.swing.JButton;
import org.apache.commons.lang3.StringUtils;
/**
 *
 * @author mbajdek
 */
public class Board extends javax.swing.JFrame {
    
    JButton[][] board;
    static Integer possibleMoves = 0;
    static boolean isInGame = true;

    /**
     * Creates new form Board
     */
    public Board() {
        initComponents();
        initializeBoard();
    }
    
    public void initializeBoard(){
        this.board = new JButton[3][3];
        this.board[0][0] = button00;
        this.board[0][1] = button01;
        this.board[0][2] = button02;
        this.board[1][0] = button10;
        this.board[1][1] = button11;
        this.board[1][2] = button12;
        this.board[2][0] = button20;
        this.board[2][1] = button21;
        this.board[2][2] = button22;
    }
    
    private String[] getCoordinates(java.awt.event.ActionEvent evt){
        
        JButton buttonClkd = (JButton)evt.getSource();
        
        return buttonClkd.getName().split("-");
    }
    
    private void makeAMove(java.awt.event.ActionEvent evt){
        
        String[] coordinates = getCoordinates(evt);
        
        Integer x = Integer.valueOf(coordinates[0]);
        Integer y = Integer.valueOf(coordinates[1]);

        if( checkIfMovePossible( x , y )){
        
            this.board[x][y].setText("X");
            possibleMoves++;
            hasGameWinner();
            
            makeOpponentMove();
            
            
        }
        else{
            System.out.println("to nie jest dozwolony ruch");
        }

                
        System.out.println("current coord " + coordinates[0] + ">" + coordinates[1]);
        
    }
    
    private String hasGameWinner(){
        
        
        if( checkHorizontal("X") || checkDiagonal("X") || checkVertical("X") ){
            isInGame = false;
            feedback.setText("The winner is X");
        }
        if( checkHorizontal("O") || checkDiagonal("O") || checkVertical("O") ){
            isInGame = false;
            feedback.setText("The winner is O");
        }
        

        

        
        return null;
    }
    
    private Boolean checkHorizontal( String type ){
        
       Boolean line1 = this.board[0][0].getText().equals(type) 
               && this.board[0][1].getText().equals(type)
               && this.board[0][2].getText().equals(type);
       
       
       Boolean line2 = this.board[1][0].getText().equals(type) 
               && this.board[1][1].getText().equals(type)
               && this.board[1][2].getText().equals(type);
       
        Boolean line3 = this.board[2][0].getText().equals(type) 
               && this.board[2][1].getText().equals(type)
               && this.board[2][2].getText().equals(type);
        
        
        return (line1 || line2 || line3);
    }
    
        private Boolean checkVertical( String type ){
        
       Boolean line1 = this.board[0][0].getText().equals(type) 
               && this.board[1][0].getText().equals(type)
               && this.board[2][0].getText().equals(type);
       
       
       Boolean line2 = this.board[0][1].getText().equals(type) 
               && this.board[1][1].getText().equals(type)
               && this.board[2][1].getText().equals(type);
       
        Boolean line3 = this.board[0][2].getText().equals(type) 
               && this.board[1][2].getText().equals(type)
               && this.board[2][2].getText().equals(type);
        
        
        return (line1 || line2 || line3);
    }
    
    private Boolean checkDiagonal( String type ){
        
       Boolean line1 = this.board[0][0].getText().equals(type) 
               && this.board[1][1].getText().equals(type)
               && this.board[2][2].getText().equals(type);
       
       
       Boolean line2 = this.board[2][0].getText().equals(type) 
               && this.board[1][1].getText().equals(type)
               && this.board[0][2].getText().equals(type);
        
        
        return (line1 || line2);
    }
    
    
    
    private boolean checkIfMovePossible( Integer x , Integer y ){
        
              
        
        if( this.board[x][y] != null && StringUtils.isEmpty(this.board[x][y].getText()) && isInGame ){
                         
               return true;
        }
        
        
        return false;
    }
    
    private void makeOpponentMove(){
        
          Integer x;
          Integer y;
          
          
            x = 0 + (int)(Math.random() * ((2 - 0) + 1));
            y = 0 + (int)(Math.random() * ((2 - 0) + 1));
            
            
            
            if(checkIfMovePossible(x, y)){
            
                this.board[x][y].setText("O");
                possibleMoves++;
                hasGameWinner();
                
            }
            else{
                
                try{
                    makeOpponentMove();
                }
                catch(StackOverflowError e){
                
                }
                
            }
          
           
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jInternalFrame1 = new javax.swing.JInternalFrame();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jPanel1 = new javax.swing.JPanel();
        feedback = new javax.swing.JLabel();
        button00 = new javax.swing.JButton();
        button01 = new javax.swing.JButton();
        button02 = new javax.swing.JButton();
        button12 = new javax.swing.JButton();
        button11 = new javax.swing.JButton();
        button10 = new javax.swing.JButton();
        button20 = new javax.swing.JButton();
        button21 = new javax.swing.JButton();
        button22 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jCheckBoxMenuItem2 = new javax.swing.JCheckBoxMenuItem();

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("jCheckBoxMenuItem1");

        jMenuItem1.setText("jMenuItem1");

        jInternalFrame1.setVisible(true);

        javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jInternalFrame1Layout.setVerticalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(127, 127, 127)
                .addComponent(feedback)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(feedback)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        button00.setAlignmentY(0.0F);
        button00.setIconTextGap(2);
        button00.setMaximumSize(new java.awt.Dimension(90, 90));
        button00.setMinimumSize(new java.awt.Dimension(90, 90));
        button00.setName("0-0"); // NOI18N
        button00.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button00ActionPerformed(evt);
            }
        });

        button01.setAlignmentY(0.0F);
        button01.setMaximumSize(new java.awt.Dimension(90, 90));
        button01.setMinimumSize(new java.awt.Dimension(90, 90));
        button01.setName("0-1"); // NOI18N
        button01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button01ActionPerformed(evt);
            }
        });

        button02.setAlignmentY(0.0F);
        button02.setMaximumSize(new java.awt.Dimension(90, 90));
        button02.setMinimumSize(new java.awt.Dimension(90, 90));
        button02.setName("0-2"); // NOI18N
        button02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button02ActionPerformed(evt);
            }
        });

        button12.setAlignmentY(0.0F);
        button12.setMaximumSize(new java.awt.Dimension(90, 90));
        button12.setMinimumSize(new java.awt.Dimension(90, 90));
        button12.setName("1-2"); // NOI18N
        button12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button12ActionPerformed(evt);
            }
        });

        button11.setAlignmentY(0.0F);
        button11.setMaximumSize(new java.awt.Dimension(90, 90));
        button11.setMinimumSize(new java.awt.Dimension(90, 90));
        button11.setName("1-1"); // NOI18N
        button11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button11ActionPerformed(evt);
            }
        });

        button10.setAlignmentY(0.0F);
        button10.setMaximumSize(new java.awt.Dimension(90, 90));
        button10.setMinimumSize(new java.awt.Dimension(90, 90));
        button10.setName("1-0"); // NOI18N
        button10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button10ActionPerformed(evt);
            }
        });

        button20.setAlignmentY(0.0F);
        button20.setMaximumSize(new java.awt.Dimension(90, 90));
        button20.setMinimumSize(new java.awt.Dimension(90, 90));
        button20.setName("2-0"); // NOI18N
        button20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button20ActionPerformed(evt);
            }
        });

        button21.setAlignmentY(0.0F);
        button21.setMaximumSize(new java.awt.Dimension(90, 90));
        button21.setMinimumSize(new java.awt.Dimension(90, 90));
        button21.setName("2-1"); // NOI18N
        button21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button21ActionPerformed(evt);
            }
        });

        button22.setAlignmentY(0.0F);
        button22.setMaximumSize(new java.awt.Dimension(90, 90));
        button22.setMinimumSize(new java.awt.Dimension(90, 90));
        button22.setName("2-2"); // NOI18N
        button22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button22ActionPerformed(evt);
            }
        });

        jMenu1.setText("Opcje");

        jCheckBoxMenuItem2.setSelected(true);
        jCheckBoxMenuItem2.setText("Restartuj");
        jCheckBoxMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jCheckBoxMenuItem2);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(button20, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(button21, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(button10, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(button11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(button00, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(button01, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(button02, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                            .addComponent(button12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(button22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(button00, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(button01, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(button02, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(button10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(button11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(button12, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(button20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(button21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(button22, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void button00ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button00ActionPerformed
        // TODO add your handling code here:
        makeAMove(evt);
        
    }//GEN-LAST:event_button00ActionPerformed

    private void button11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button11ActionPerformed
        // TODO add your handling code here:
        makeAMove(evt);
    }//GEN-LAST:event_button11ActionPerformed

    private void button22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button22ActionPerformed
        // TODO add your handling code here:
        makeAMove(evt);

        
        

    }//GEN-LAST:event_button22ActionPerformed

    private void button01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button01ActionPerformed
        // TODO add your handling code here:
        makeAMove(evt);
    }//GEN-LAST:event_button01ActionPerformed

    private void button02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button02ActionPerformed
        // TODO add your handling code here:
        makeAMove(evt);
    }//GEN-LAST:event_button02ActionPerformed

    private void button10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button10ActionPerformed
        // TODO add your handling code here:
        makeAMove(evt);
    }//GEN-LAST:event_button10ActionPerformed

    private void button12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button12ActionPerformed
        // TODO add your handling code here:
        makeAMove(evt);
    }//GEN-LAST:event_button12ActionPerformed

    private void button20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button20ActionPerformed
        // TODO add your handling code here:
        makeAMove(evt);
    }//GEN-LAST:event_button20ActionPerformed

    private void button21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button21ActionPerformed
        // TODO add your handling code here:
        makeAMove(evt);
    }//GEN-LAST:event_button21ActionPerformed

    private void jCheckBoxMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem2ActionPerformed
        // TODO add your handling code here:
        
        
        isInGame = true;
        feedback.setText("");
        for( int i = 0 ; i < this.board.length ; i++ ){
            for( int j = 0 ; j < this.board[i].length ; j++ ){
                 this.board[i][j].setText("");
         }
        }
        
        
        
    }//GEN-LAST:event_jCheckBoxMenuItem2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Board().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton button00;
    private javax.swing.JButton button01;
    private javax.swing.JButton button02;
    private javax.swing.JButton button10;
    private javax.swing.JButton button11;
    private javax.swing.JButton button12;
    private javax.swing.JButton button20;
    private javax.swing.JButton button21;
    private javax.swing.JButton button22;
    private javax.swing.JLabel feedback;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem2;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu1;
    // End of variables declaration//GEN-END:variables
}
